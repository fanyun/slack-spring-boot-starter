# slack-sprint-boot-starter

#### yml配置样例

```yml
cyberx:
  slack: # 主要通过 bot.id + channel.id 唯一定位消息发送端点
    bot-config-list:
      - id: "U065*******" # 机器人ID
        name: "bot-name****" # 机器人名称
        token: "xoxb-*****************************************************" # start with 'xoxb-'
      - id: "U02K*******"
        name: "bot-name****"
        token: "xoxb-*****************************************************"
    channel-config-list:
      - id: "C065*******" # 频道 ID
        name: "channel-name****" # 频道名称
      - id: "C02E*******"
        name: "channel-name****"
     ...
```

#### java 模块化配置

```java
 module X {
        requires slack.spring.boot.starter; // 引入 slack.starter
}
```

#### 命令行格式输出 table

![img.png](img.png)

