# slack-sprint-boot-starter

#### Description

```yml
cyberx:
  slack: # unique by bot.id + channel.id 
    bot-config-list:
      - id: "U065*******" # bot id
        name: "bot-name****" # bot name
        token: "xoxb-*****************************************************" # start with 'xoxb-'
      - id: "U02K*******"
        name: "bot-name****"
        token: "xoxb-*****************************************************"
    channel-config-list:
      - id: "C065*******" # channel id
        name: "channel-name****" # channel name
      - id: "C02E*******"
        name: "channel-name****"
      ...
```

#### java module config

```java
 module X {
        requires slack.spring.boot.starter; // require slack.starter
}
```

