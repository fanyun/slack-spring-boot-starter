package com.cyberx.slackspringbootstarter.util;

import com.cyberx.slackspringbootstarter.model.table.Align;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class StringUtils {
  private StringUtils() {}

  /**
   * 将字符串按照居中方式填充。
   *
   * @param string 待填充字符
   * @param length 填充长度
   * @param symbol 填充符号
   * @return 填充后的字符串
   */
  public static String padding(String string, int length, char symbol) {
    return padding(string, length, symbol, Align.MID);
  }

  /**
   * 将字符串填充指定长度并指定对齐方式。
   *
   * @param string 待填充字符
   * @param length 填充长度
   * @param symbol 填充符号
   * @param align 对齐方式
   * @return 填充后的字符串
   */
  public static String padding(String string, int length, char symbol, Align align) {
    return switch (align) {
      case Align.LEFT -> padLeft(string, length, symbol);
      case Align.RIGHT -> padRight(string, length, symbol);
      default -> padMid(string, length, symbol);
    };
  }

  /**
   * 将 content 按照正则匹配，返回可以匹配的字符串列表。
   *
   * @param reg 正则
   * @param content 待匹配数据
   * @return 字符串列表
   */
  public static List<String> extractMessage(String reg, String content) {
    Pattern compile = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
    Matcher matcher = compile.matcher(content);
    List<String> list = new ArrayList<>();
    while (matcher.find()) {
      list.add(matcher.group());
    }
    return list;
  }

  /**
   * 将字符串 string 重复 count 次。
   *
   * @param string 待重复字符串
   * @param count 重复次数
   * @return 格式化后的字符串
   */
  public static String repeat(String string, int count) {
    StringBuilder stringBuilder = new StringBuilder();
    IntStream.range(0, count).forEach(i -> stringBuilder.append(string));
    return stringBuilder.toString();
  }

  public static int consoleLength(String string) {
    if (isEmpty(string)) {
      return 0;
    }
    int length = 0;
    char[] charArray = string.toCharArray();
    for (char c : charArray) {
      if (Character.isIdeographic(c)) {
        length += 2;
      } else {
        length += 1;
      }
    }
    return length;
  }

  public static boolean isEmpty(String string) {
    return string == null || string.isEmpty();
  }

  /**
   * 字符串居中。
   *
   * @param string 待居中字符串
   * @param length 填充长度
   * @param symbol 填充符号
   * @return 填充后的字符串
   */
  private static String padLeft(String string, int length, char symbol) {
    if (string.length() >= length) {
      return string;
    }

    int consoleLength = consoleLength(string);
    StringBuilder sb = new StringBuilder(length);
    sb.append(string);
    for (int i = consoleLength; i < length; i++) {
      sb.append(symbol);
    }
    return sb.toString();
  }

  /**
   * 字符串居中。
   *
   * @param string 待居中字符串
   * @param length 填充长度
   * @param symbol 填充符号
   * @return 填充后的字符串
   */
  private static String padRight(String string, int length, char symbol) {
    if (string.length() >= length) {
      return string;
    }
    int consoleLength = consoleLength(string);
    StringBuilder sb = new StringBuilder(length);
    for (int i = 0; i < (length - consoleLength); i++) {
      sb.append(symbol);
    }
    sb.append(string);
    return sb.toString();
  }

  /**
   * 字符串居中。
   *
   * @param string 待居中字符串
   * @param length 填充长度
   * @param symbol 填充符号
   * @return 填充后的字符串
   */
  private static String padMid(String string, int length, char symbol) {
    if (string.length() >= length) {
      return string;
    }
    int consoleLength = consoleLength(string);
    int beginLength = (length - consoleLength) / 2;
    // 填充前部分
    return String.valueOf(symbol).repeat(beginLength)
        + string
        +
        // 填充后半部分
        String.valueOf(symbol).repeat(Math.max(0, length - (beginLength + consoleLength)));
  }
}
