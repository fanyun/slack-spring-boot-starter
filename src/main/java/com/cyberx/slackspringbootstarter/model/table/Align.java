package com.cyberx.slackspringbootstarter.model.table;

/** 对齐方式 */
public enum Align {
  /** 左对齐 */
  LEFT,
  /** 剧中对齐 */
  MID,
  /** 右对齐 */
  RIGHT,
  ;
}
