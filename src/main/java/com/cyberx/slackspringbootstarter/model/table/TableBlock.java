package com.cyberx.slackspringbootstarter.model.table;

import com.cyberx.slackspringbootstarter.util.ConsoleTable;
import com.slack.api.model.block.RichTextBlock;
import com.slack.api.model.block.element.BlockElement;
import com.slack.api.model.block.element.RichTextElement;
import com.slack.api.model.block.element.RichTextPreformattedElement;
import com.slack.api.model.block.element.RichTextSectionElement;
import java.util.List;

/** 构造slack富文本格式的table输出： */
public class TableBlock extends RichTextBlock {

  public static RichTextBlock of(String tableName, String[] header, String[][] data) {
    // 表数据
    BlockElement tableDataBlock = tableBlock(tableName, header, data);
    return RichTextBlock.builder().elements(List.of(tableDataBlock)).build();
  }

  private static RichTextPreformattedElement tableBlock(
      String tableName, String[] header, String[][] data) {
    String tableFormatString = ConsoleTable.of(tableName, header, data);
    RichTextElement textElement = rickTextElementList(tableFormatString);
    return RichTextPreformattedElement.builder().elements(List.of(textElement)).build();
  }

  private static RichTextElement rickTextElementList(String text) {
    return RichTextSectionElement.Text.builder().text(text).build();
  }
}
