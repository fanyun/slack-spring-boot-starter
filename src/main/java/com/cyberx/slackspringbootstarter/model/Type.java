package com.cyberx.slackspringbootstarter.model;

import lombok.Getter;

public enum Type {
  /** Header */
  HEADER("header"),
  /** Divider */
  DIVIDER("divider"),
  /** plain text */
  PLAIN_TEXT("plain_text"),
  ;

  @Getter private final String code;

  Type(String code) {
    this.code = code;
  }
}
