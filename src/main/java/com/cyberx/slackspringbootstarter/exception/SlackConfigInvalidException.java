package com.cyberx.slackspringbootstarter.exception;

/** Slack配置非法异常 */
public class SlackConfigInvalidException extends Exception {}
