package com.cyberx.slackspringbootstarter.properties;

import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/** 属性配置 */
@ConfigurationProperties(prefix = "cyberx.slack")
public class SlackProperties {
  /** 机器人配置信息 */
  @NestedConfigurationProperty private List<BotConfig> botConfigList;

  /** 频道配置信息 */
  @NestedConfigurationProperty private List<ChannelConfig> channelConfigList;

  public List<BotConfig> getBotConfigList() {
    return botConfigList;
  }

  public void setBotConfigList(List<BotConfig> botConfigList) {
    this.botConfigList = botConfigList;
  }

  public List<ChannelConfig> getChannelConfigList() {
    return channelConfigList;
  }

  public void setChannelConfigList(List<ChannelConfig> channelConfigList) {
    this.channelConfigList = channelConfigList;
  }
}
