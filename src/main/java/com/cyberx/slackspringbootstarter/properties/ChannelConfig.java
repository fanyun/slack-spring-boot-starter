package com.cyberx.slackspringbootstarter.properties;

/** 频道配置 */
public class ChannelConfig {
  /** 频道ID */
  private String id;

  /** 频道名称 */
  private String name;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
