package com.cyberx.slackspringbootstarter.properties;

/** 机器人配置，一个频道内一个机器人是一个实例 */
public class BotConfig {
  /** 机器人Id */
  private String id;

  /** 机器人名称 */
  private String name;

  /** 机器人token start with 'xoxb-' */
  private String token;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
