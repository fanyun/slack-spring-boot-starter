package com.cyberx.slackspringbootstarter.config;

import com.cyberx.slackspringbootstarter.properties.SlackProperties;
import com.cyberx.slackspringbootstarter.service.Slack;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(SlackProperties.class)
@EnableConfigurationProperties(SlackProperties.class)
public class AutoConfiguration {
  private final SlackProperties properties;

  public AutoConfiguration(SlackProperties properties) {
    this.properties = properties;
  }

  @Bean
  @ConditionalOnMissingBean
  public Slack slack() {
    return new Slack(properties);
  }
}
