package com.cyberx.slackspringbootstarter.service;

import com.cyberx.slackspringbootstarter.model.table.TableBlock;
import com.cyberx.slackspringbootstarter.properties.BotConfig;
import com.cyberx.slackspringbootstarter.properties.ChannelConfig;
import com.slack.api.Slack;
import com.slack.api.model.block.RichTextBlock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
class ApiTest {
  private Api api;

  @BeforeEach
  void setUp() {
    com.slack.api.Slack slack = new Slack();
    ChannelConfig channelConfig = new ChannelConfig();
    channelConfig.setName("shiji-alert");
    channelConfig.setId("C039SEH08JC");
    BotConfig botConfig = new BotConfig();
    botConfig.setId("A02GW7AL5CG");
    botConfig.setName("Gewu");
    botConfig.setToken("xoxb-2286321686112-2552798134676-M3xz4vUyU9433i2lVycbIFVq");
    api = new Api(slack, channelConfig, botConfig);
  }

  @Test
  void send() {
    api.send("message");
  }

  @Test
  void testSend() {
    RichTextBlock richTextBlock =
        TableBlock.of(
            "test",
            new String[] {"id", "CreditStatus", "AccountGroup", "Venue", "AccountType"},
            new String[][] {
              {"1", "", "bybit_mm2_ro", "BYBIT_PERP"},
              {"2", "", "bybit_mm3_ro", "BYBIT_SPOT", "SPOT", "action"}
            });
    api.send(richTextBlock);
  }
}
