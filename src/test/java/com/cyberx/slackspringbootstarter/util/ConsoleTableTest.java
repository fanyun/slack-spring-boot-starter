package com.cyberx.slackspringbootstarter.util;

import static org.junit.jupiter.api.Assertions.*;

import com.cyberx.slackspringbootstarter.model.table.TableBlock;
import com.cyberx.slackspringbootstarter.properties.BotConfig;
import com.cyberx.slackspringbootstarter.properties.ChannelConfig;
import com.cyberx.slackspringbootstarter.service.Api;
import com.slack.api.Slack;
import com.slack.api.model.block.RichTextBlock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConsoleTableTest {
  private static final String[] header = {
    "id",
    "CreditStatus",
    "AccountGroup",
    "Venue",
    "AccountType",
    "DownLimit",
    "UpLimit",
    "ModelLTV",
    "ZhouyiLTV",
    "ExchLTV",
    "ExchMgnRatio",
    "ExchCreditline",
    "ExchEq",
    "PriceUpdateTime"
  };
  private static final String[][] data = {
    {
      "1",
      "",
      "bybit_mm2_ro",
      "BYBIT_PERP",
      "ALL_CROSSED",
      "0.0",
      "42904.67",
      "0.76",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "Fri Nov 17 05:11:47 GMT 2023"
    },
    {
      "2",
      "",
      "bybit_mm2_ro",
      "BYBIT_PERP",
      "ALL_CROSSED",
      "0.0",
      "57812.21",
      "0.3",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "0.0",
      "Fri Nov 17 05:11:47 GMT 2023"
    }
  };

  private Api api;

  @BeforeEach
  void setUp() {
    com.slack.api.Slack slack = new Slack();
    ChannelConfig channelConfig = new ChannelConfig();
    channelConfig.setName("shiji-alert");
    channelConfig.setId("C039SEH08JC");
    BotConfig botConfig = new BotConfig();
    botConfig.setId("A02GW7AL5CG");
    botConfig.setName("Gewu");
    botConfig.setToken("xoxb-2286321686112-2552798134676-M3xz4vUyU9433i2lVycbIFVq");
    api = new Api(slack, channelConfig, botConfig);
  }

  @Test
  void of() {
    String table = ConsoleTable.of(null, header, data);
    System.out.println(table);
    RichTextBlock richTextBlock =
            TableBlock.of(
                    "test",
                    header,
                    data);
    api.send(richTextBlock);
    assertNotNull(table);
  }
}
