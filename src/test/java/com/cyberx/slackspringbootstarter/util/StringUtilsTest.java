package com.cyberx.slackspringbootstarter.util;

import static org.junit.jupiter.api.Assertions.*;

import com.cyberx.slackspringbootstarter.model.table.Align;
import org.junit.jupiter.api.Test;

class StringUtilsTest {

  @Test
  void padding() {
    String string = "Abc";
    char symbol = '*';
    int length = 5;

    String paddingLeft = StringUtils.padding(string, length, symbol, Align.LEFT);
    assertEquals("Abc**", paddingLeft);
    String padding = StringUtils.padding(string, length, symbol);
    assertEquals("*Abc*", padding);
    String paddingRight = StringUtils.padding(string, length, symbol, Align.RIGHT);
    assertEquals("**Abc", paddingRight);
  }

  @Test
  void paddingWithZH() {
    String string = "汉";
    char symbol = '*';
    int length = 5;

    String paddingLeft = StringUtils.padding(string, length, symbol, Align.LEFT);
    assertEquals("汉***", paddingLeft);
    String padding = StringUtils.padding(string, length, symbol);
    assertEquals("*汉**", padding);
    String paddingRight = StringUtils.padding(string, length, symbol, Align.RIGHT);
    assertEquals("***汉", paddingRight);
  }
}
